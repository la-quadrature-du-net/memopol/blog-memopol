# blog-memopol

Clone the project::

    git clone --recursive gitlab@git.laquadrature.net:political-memory/blog-memopol.git

Install pelican::

    pip install pelican
    # Or use apt or your favorite package manager

If you have this error::

    CRITICAL: UndefinedError: 'extra_siteurls' is undefined

Then it means that the plugins directory is not populated, to do so::

    git submodule update --init
    # Or, next time do git clone with --recursive

The command to generate the output is::

    pelican
    # Or, if you're developping, use this command and open localhost:8000
    make devserver


# theme

The theme is based on bootstrap4. To hack it::

    cd theme

    # Install dependencies from package.json into ./node_modules
    npm install

    # Build scss/* and js/* into static/dist
    ./node_modules/.bin/gulp

    # Then you need to run the pelican command again in the parent directory
    cd ../ && pelican

# plugins

Don't forget to checkout submodules, ie. with
``git submodule sync; git submodule init --update``
