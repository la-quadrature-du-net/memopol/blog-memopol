#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os

AUTHOR = u'Memopol Team'
SITENAME = u'Memopol Project'
SITEURL = 'https://memopol.org/'
SITETITLE= 'Bridging the gap between politics and citizens'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'
LOCALE = u'en_EN.utf8'
DEFAULT_DATE_FORMAT = '%d/%m/%Y'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('La Quadrature du Net', 'https://www.laquadrature.net'),
         ('LQDN\'s Memopol Instance', 'http://lqdn.memopol.eu/'),
        )

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/memopol2'),
         )

DEFAULT_PAGINATION = 5

#menu
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True

PLUGIN_PATHS = ["plugins", os.path.join(os.path.dirname(__file__), 'plugins')]
PLUGINS = [
    'i18n_subsites',
]

I18N_SUBSITES = {
    'fr': {
        'SITENAME': 'Site officiel de Memopol',
        'LOCALE': 'fr',
        'SITEURL': '/fr',
        'DEFAULT_CATEGORY': "actualités"
    },
}

#Arthur 16/10/2016 Je ne suis pas sur de l'utilite
I18N_UNTRANSLATED_ARTICLES = 'remove'
I18N_UNTRANSLATED_PAGES = 'remove'

I18N_GETTEXT_LOCALEDIR = 'translations/'
I18N_GETTEXT_DOMAIN = 'messages'

languages_lookup = {
    'en': 'English',
    'fr': 'Français',
}

def lookup_lang_name(lang_code):
    return languages_lookup[lang_code]


def my_ordered_items(dict):
    items = list(dict.items())
    # swap first and last using tuple unpacking
    items[0], items[-1] = items[-1], items[0]
    return items


JINJA_FILTERS = {
    'lookup_lang_name': lookup_lang_name,
    'my_ordered_items': my_ordered_items,
}

JINJA_EXTENSIONS = ['jinja2.ext.i18n']

#Theme
THEME = os.path.join(os.path.dirname(__file__), 'theme')

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

DEFAULT_CATEGORY = 'News'
