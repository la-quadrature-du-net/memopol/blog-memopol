$(document).ready(function() {
  function hideMenuOnMobile() {
    if ($(window).width() < 768) {
      $('aside .menus').hide();
    } else {
      $('aside .menus').show();
    }
  }

  $(window).resize(hideMenuOnMobile);

  $('.toggle-menu').on('click', function () {
    $('aside .menus').slideToggle(100, 'linear', function() {
      if ($(this).is(':visible'))
        $(this).css('display','inline-block');
    });
  });
});
