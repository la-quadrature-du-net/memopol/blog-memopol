var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();

gulp.task('fonts', function () {
  return gulp.src('fonts.list')
    .pipe(plugins.googleFontsBase64Css())
    .pipe(plugins.concat('_fonts.scss'))
    .pipe(gulp.dest('scss'))
  ;
});

gulp.task('styles', function() {
  return gulp.src('node_modules/bootstrap/scss/bootstrap.scss')
    .pipe(plugins.customizeBootstrap('scss/*.scss'))
    .pipe(plugins.plumber())
    //.pipe(plugins.sourcemaps.init())
    .pipe(plugins.sass().on('error', plugins.sass.logError))
    //.pipe(plugins.sourcemaps.write('.'))
    .pipe(plugins.autoprefixer({browsers: ['last 1 version']}))
    .pipe(plugins.rename({ suffix: '.min' }))
    .pipe(gulp.dest('static/dist'))
    .pipe(plugins.minifyCss())
    .pipe(gulp.dest('static/dist'))
  ;
});

gulp.task('scripts', function() {
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/tether/dist/js/tether.js',
    'node_modules/bootstrap/dist/js/bootstrap.js',
    'node_modules/chart.js/dist/Chart.bundle.js',
    'js/main.js',
  ])
  .pipe(plugins.concat('script.js'))
  .pipe(gulp.dest('static/dist'));
});

gulp.task('default', [
  'fonts',
  'scripts',
  'styles'
]);
