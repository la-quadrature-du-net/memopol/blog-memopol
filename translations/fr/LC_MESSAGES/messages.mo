��          �            x    y  o   �  _     j   a  I   �  r     �   �  �     �   �  c   �  Y   �     O  1   [  '   �     �  �  �  +  e	  �   �
  i     �   �  Z     �   t       �   �  �   z  �   -  [   �  	     :     9   Z  !   �        	   
                                                                    
    Citizens can already access the <a
    href="https://memopol.lqdn.fr">Memopol installation</a> that is managed
    by <a href="https://laquadrature.net">La Quadrature du Net</a>, which
    is used by other organizations such as <a
    href="https://edri.org/">EDRi</a>.
     
    Memopol builds a database from the parliaments including representatives, votes, dossiers, and more !
     
    Memopol provides activists the ability to track activity of political representatives
     
    Memopol uses the <b>public</b> data from parliaments, but secret ballots can be added manually !
     
    Memopol uses vote data to compute scores of each representative
     
    This chart represents numbers of public votes against the number of
    votes made using secret ballots.
     
    This graphic speaks for itself: very little is shared by your
    representatives about the votes taken in the European Parliament.
     
    We vote for a new representative with: their party's program, their
    communication campaign, the promises they made, our historical attachment...
    And sometimes, we might even remember a few things they've done !
     
    You can install Memopol to build your own database of representatives,
    dossiers, documents, proposals and votes from various chambers:
     
The human memory is bad, really bad, we are barely capable
of remembering what we ate 3 weeks ago
 
from the <span class="green">past</span>
<br />
for the <span class="red">future</span>
 Get started Share and sharpen your skills in the DevOps Guild Share your passion for solving problems hacktivist, creator of Memopol Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2016-10-16 14:11+0200
PO-Revision-Date: 2016-10-16 14:34+0200
Language: fr
Language-Team: fr <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.3.4
Last-Translator: 
X-Generator: Poedit 1.8.7.1
 
   Les citoyens peuvent déjà avoir accès à l'<a
    href="https://memopol.lqdn.fr">installation Memopol</a> qui est gérée
    par <a href="https://laquadrature.net">La Quadrature du Net</a>, qui
    est utilisée par d'autres organisations telles que <a
    href="https://edri.org/">EDRi</a>. 
    Memopol construit une base de donnée des différentes parlements incluant les représentants, les votes, les dossiers, et plus encore ! 
    Memopol fournit aux activistes la capacité de suivre l'activité des représentants politiques
     
    Memopol utilise les  données <b>publiques</b> des parlements, mais les votes à bulletin secret peuvent être ajouté manuellement !
     
    Memopol utilise les données de vote pour calculer les scores de chaque représentant 
    Ce graphique représente le nombre de votes rendus public par rapport au nombre de
    votes effectués utilisant des bulletins secrets.
     
    Ce graphique parle de lui-même : peu est partagé par nos
    représentants des votes tenus au Parlement Européen.
     
   Nous votons pour un représentant avec : le programme du parti, leur
    communication de campagne, leurs promesses faites, notre attachement historique
    Et parfois, on peut arriver à se souvenir de quelques choses qu'ils ont faites ! 
    Vous pouvez installer Memopol pour construire votre propre base de données de représentants,
    dossiers, documents, propositions et votes des différentes chambres:
     
La mémoire humaine est mauvaise, très mauvaise, nous sommes à peine capable
de se souvenir de ce que nous avons fait il y a 3 semaines
 
from the <span class="green">passé</span>
<br />
for the <span class="red">future</span>
 Commencé Partager and affiner vos compétences dans le DevOps Guild Partager votre passion pour la résolution des problèmes hacktiviste, créateur de Memopol 