Title: Where do we go from here?
Date: 2017-09-18 12:00
Category: News
Author: okhin
Lang: en


# Ongoing development
Long time no see. The development on memopol has been slow those last months,
mainly due to a lack of time from core developpers who needs to go on with their
life.

So, if you know and like the project and if you've read the code and want to help
here are some of the work in progress issues.

## Better search function
We're trying to have a better search function. We've started to integrate solr
into the project, and it is now working for self completion on representatives.
This is done using haystack and we'd like to be able to search and filter those
search by active mandates and to add a search index for proposals and dossiers.

## Ease the recommandation process
For now the recommandation process is quite complex, at least from a UX point of
view. It is also based on django-suits which is not GPL compliant, so there's
some work we need to be doing here.

## Finish the views proposals
New templates where proposed by Ag3m to be integrated to display a better
understanding of what's happening in the parliaments.

All this work in progress is documented on our [gitlab](https://git.laquadrature.net/memopol/memopol/issues)
so feel free to register and to help there.

# Improving datasources
Memopol relies on other project to get its data. Francedata is an importer/formatter
for the french chambers, andit get its data both from the official websites and
from [Regards Citoyens](https://www.regardscitoyens.org/). FranceData is more a
bunch of adhoc scrappers than a real project. It should be soon integrated into
RegardsCitoyens API which will allow us to remove our dependency to FranceData.

At the european level, we get our data from parltrack, which is planning on
releasing a new version. We need this 2.0 version to bring the european parliament
and what's happning there to a wider audience.

So, there's a lot to be done yet, if you know about django and can work autonomously
on the code, feel free to join #lqdn-memopol@Freenode