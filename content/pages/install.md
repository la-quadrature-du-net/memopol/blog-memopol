Title: Download & Install instructions
Lang: en

is_null wrote a pretty (and up-to-date) documentation here : [https://memopol.lqdn.fr/docs/](https://memopol.lqdn.fr/docs/).
This should help you to install Memopol as a development instance on your computer.
