Title: Contribute
Lang: fr

# Où est le code ?

Tout le code est <a href="https://git.laquadrature.net/political-memory">ici</a>!

# Où est la documentation ?

Elle peut être trouvée <a href="https://memopol.lqdn.fr/docs/">ici</a>!

# Comment puis-je aider ?

Aidez-nous à améliorer Memopol en contribuant au code ! C'est en Python et Django.

Vous pouvez également reporter un bug <a href="https://git.laquadrature.net/groups/memopol/issues">ici</a>.

# Feuille de travail

## Deux objectifs
* Nous voulons que le maximum de personnes puissent aider, donc nous voulons faciliter l'installation et la documentation
* Séparer plus le core des modules : nous voulons que d'autres associations puissent utiliser Memopol, pas seulement nous

## Nos prochaines étapes
- Un redesign complet de la page d'accueil afin que le site web du projet puisse :
    - être un tremplin utile pour les citoyens qui découvrent l'outil *et* les activistes expérimentés
    - être visuellement attrayant
    - montrer des données utiles (dataviz)
    - rendre évident rapidement ce qu'est Memopol et quel est son but
    - offrir la possibilité de poster facilement une nouvelle "position publique" d'un MEP.
- Un redesign des pages des députés européens ou MEP (Member of the European Parliament) avec :
    - la vue d'ensemble de chaque MEP, avec des informations administratives
    - le score des MEP 
    - les dernières positions publiques avec lien vers une liste exhaustive
    - la visualisation des données du score des MEP (beaux graphes, évolution du score...) (dataviz)
    - un widget avec un résumé rapide de chaque MEP qui montre l'information la plus importante et qui peut être aisément réutilisable par d'autres sites.

Une feuille de route plus précise peut être trouvée [ici](https://wiki.laquadrature.net/Projects/Memopol/Roadmap/fr).
