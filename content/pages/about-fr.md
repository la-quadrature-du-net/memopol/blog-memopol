Title: À propos de Memopol
Lang: fr

Mémoire Politique est un outil conçu pour aider les citoyen(ne)s à suivre l'activité et les votes de leurs député(e)s européen(ne)s, et les aider à influencer leurs décisions et leurs politiques en facilitant au maximum leur mise en relation.

L'idée est simple : beaucoup de décisions sont prises au Parlement européen (et aux parlements nationaux) en sachant que la plupart des citoyen(ne)s ne seront pas au courant. En accentuant la surveillance des votes des député(e)s européen(ne)s, nous pouvons augmenter le coût politique de ces décisions et remettre les citoyen(ne)s au cœur de la vie politique.

Mémoire politique ambitionne de simplifier ce travail au maximum en donnant aux citoyen(ne)s une interface facile à utiliser pour visualiser les votes de leurs représentant(e)s, écrire des questions, suivre leurs élu(e)s dans le temps, et en leur donnant les moyens de contacter rapidement un(e) député(e) européen(ne) en listant toutes les informations utiles à un seul endroit.

Plus précisément, Mémoire politique est un logiciel qui peut être utilisé par tout le monde pour créer un site web afin de suivre leurs député(e)s européen(ne)s sur le sujet de leur choix, et se rappeler de ce qu'iels ont dit et ce qu'iels ont fait, en plus de les évaluer en fonction de critères biens précis.

C'est une application web écrite avec le framework Python Django, conçue pour être facilement déployée sur votre site web.

Ce projet, Memopol 2, est la version 2.0 de l'outil Mémoire Politique 1.0 écrit en Perl par la Quadrature du Net au début de l'activisme sur Internet.

Mémoire politique est du logiciel libre (sous licence Affero GPL) que tout le monde peut utiliser, partager, étudier, enrichir, déboguer, traduire, etc.

Rendez-vous sur <a href="/pages/download-install-instructions.html">Download Page</a> pour télécharger Mémoire politique, ou allez à l'exemple à <a href="https://memopol.org">l'url memopol.org</a> pour le voir en action.

