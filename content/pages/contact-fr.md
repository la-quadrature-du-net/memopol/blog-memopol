Title: Contact
Lang: fr

Vous pouvez nous contacter par irc: #lqdn-memopol sur irc.freenode.net.

Il existe également une liste de discussion, n'hésitez pas à vous abonner <a href="https://lists.laquadrature.net/cgi-bin/mailman/listinfo/mempol2">ici</a> et à partager l'information.
